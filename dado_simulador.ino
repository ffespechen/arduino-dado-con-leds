//Simulador de Dado, utilizando LEDS

const int pin1D = 10;
const int pin2D = 11;
const int pin3D = 12;

const int pin2C = 5;

const int pin3I = 7;
const int pin2I = 8;
const int pin1I = 9;


void setup() {

  pinMode(pin1D, OUTPUT);
  pinMode(pin2D, OUTPUT);
  pinMode(pin3D, OUTPUT);

  pinMode(pin2C, OUTPUT);

  pinMode(pin1I, OUTPUT);
  pinMode(pin2I, OUTPUT);
  pinMode(pin3I, OUTPUT);

  Serial.begin(9600);

}

void clearPin()
{
  digitalWrite(pin1D, LOW);
  digitalWrite(pin2D, LOW);
  digitalWrite(pin3D, LOW);

  digitalWrite(pin2C, LOW);

  digitalWrite(pin1I, LOW);
  digitalWrite(pin2I, LOW);
  digitalWrite(pin3I, LOW);
  
}

void numeroDado(int numero)
{
  clearPin();

  switch(numero)
  {
    case 1:
      digitalWrite(pin2C, HIGH);
      break;

    case 2:
      digitalWrite(pin1I, HIGH);
      digitalWrite(pin3D, HIGH);
      break;

    case 3:
      digitalWrite(pin1I, HIGH);
      digitalWrite(pin2C, HIGH);
      digitalWrite(pin3D, HIGH);
      break;

    case 4:
      digitalWrite(pin1I, HIGH);
      digitalWrite(pin3I, HIGH);
      digitalWrite(pin1D, HIGH);
      digitalWrite(pin3D, HIGH);
      break;

    case 5:
      digitalWrite(pin1I, HIGH);
      digitalWrite(pin3I, HIGH);
      digitalWrite(pin2C, HIGH);
      digitalWrite(pin1D, HIGH);
      digitalWrite(pin3D, HIGH);
      break;

    case 6:
      digitalWrite(pin1I, HIGH);
      digitalWrite(pin2I, HIGH);
      digitalWrite(pin3I, HIGH);
      digitalWrite(pin1D, HIGH);
      digitalWrite(pin2D, HIGH);
      digitalWrite(pin3D, HIGH);
      break;
  }

  
}


void test()
{
  for(int j=1; j<=6; j++)
  {
    clearPin();
    numeroDado(j);
    delay(2000);
    
  }
}

void loop() 
{
  //Función de prueba
  test();

  //Usando números aleatorios
  for(int i=0; i<100; i++)
  {
    int numero = int(random(1,7));
    numeroDado(numero);
    Serial.println(numero);
    delay(2000);
  }
  
}
