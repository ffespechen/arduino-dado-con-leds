# Arduino-dado-con-LEDs

Este proyecto simple utiliza siete (7) LEDs distribuidos como los puntos de las diferentes caras de un dado, e implementa funciones para encenderlos de acuerdo al número a representar (que corresponde al parámetro que recibe la función)

Se provee además, de una función test() para probar que todas las conexiones de los LEDs estén correctas, mostrando los números del 1 al 6.

Luego se genera un bucle para encender en forma aleatoria (con la función random()) una serie de números, que puede contrastarse con el monitor serie del IDE de Arduino.
